import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// eslint-disable-next-line no-unused-vars
import App from './App';
import * as serviceWorker from './serviceWorker';
// eslint-disable-next-line no-unused-vars
import { RequestProvider } from 'react-request-hook';
import { axiosInstance } from './config/APIConfig'


ReactDOM.render(
    <RequestProvider value={axiosInstance}>
        <App/>
    </RequestProvider>,
    document.getElementById('root')
)
// ReactDOM.render(<App />, document.getElementById('root'));
// console.log(JSON.stringify(process.env.NODE_ENV));

// if (process.env.NODE_ENV !== 'development') {
//     const {whyDidYouUpdate} = require('why-did-you-update');
//     whyDidYouUpdate(React);
//   }


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
