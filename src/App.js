// eslint-disable-next-line no-unused-vars
import React, { useReducer, useEffect, useState } from "react";
// eslint-disable-next-line no-unused-vars
import UserBar from "./user/UserBar";
// eslint-disable-next-line no-unused-vars
import CreatePost from "./post/CreatePost";
// eslint-disable-next-line no-unused-vars
import PostList from "./post/PostList";
import appReducer from "./reducers";
// eslint-disable-next-line no-unused-vars
import Header from "./Header";
// eslint-disable-next-line no-unused-vars
import { ThemeContext, StateContext } from "./contexts";
// eslint-disable-next-line no-unused-vars
import ChangeTheme from "./ChangeTheme";
import { useResource } from "react-request-hook";

// Comment stub data for Post
// Now using db
// const defaultPosts = [
//   {
//     title: "React Hooks",
//     content: "The greatest thing since sliced bread!",
//     author: "Daniel Bugl"
//   },
//   {
//     title: "Using React Fragments",
//     content: "Keeping the DOM tree clean!",
//     author: "Daniel Bugl"
//   }
// ];

export default function App() {
  const [theme, setTheme] = useState({
    primaryColor: "deepskyblue",
    secondaryColor: "coral"
  });

  // 1st step using state

  // const [user, setUser] = useState("");
  // const [posts, setPosts] = useState(defaultPosts);

  // 2nd step using reducer in appitself
  //const [user, dispatchUser] = useReducer(userReducer, "");
  // const [posts, dispatchPosts] = useReducer(postsReducer, "")

  //3rd step use Reducer in seperate file to create obj from the state
  const [state, dispatch] = useReducer(appReducer, {
    user: "",
    posts: [],
    error: ""
  });
  const { user, error } = state;

  const [posts, getPosts] = useResource(() => ({
    url: "/posts",
    method: "get"
  }));
  useEffect(getPosts, []);
  useEffect(() => {
    if (posts && posts.error) {
      dispatch({ type: "POSTS_ERROR" });
    }
    if (posts && posts.data) {
      dispatch({ type: "FETCH_POSTS", posts: posts.data.reverse() });
    }
  }, [posts]);

  // This is using fetch method without useResource hook method
  // useEffect(() => {
  //   fetch("/api/posts")
  //     .then(result => result.json())
  //     .then(posts => dispatch({ type: "FETCH_POSTS", posts }));
  // }, []);

  // This checks if USER is avilable  then
  // change the title, mount only if user is change [user]
  useEffect(() => {
    fetch("/api/posts")
      .then(result => result.json())
      .then(posts => dispatch({ type: "FETCH_POSTS", posts }));
  }, []);
  useEffect(() => {
    if (user) {
      document.title = `${user} - React hook blog`;
    } else document.title = `React hook blog`;
  }, [user]);

  return (
    <StateContext.Provider value={{ state, dispatch }}>
      <ThemeContext.Provider value={theme}>
        <div style={{ padding: 8 }}>
          <Header text={"React Hooks Blog"} />
          <ChangeTheme theme={theme} setTheme={setTheme} />
          <React.Suspense fallback={`is Loading...`}>
            <UserBar/>
          </React.Suspense>
          <br />
          {user && <CreatePost />}

          <br />
          <hr />
          {error && <b>{error}</b>}
          <PostList />
        </div>
      </ThemeContext.Provider>
    </StateContext.Provider>
  );
}
