import React, { useContext } from "react";
import { StateContext } from "../contexts";
import Post from "./Post";

export default function PostList() {
  const { state } = useContext(StateContext);
  const { posts } = state;
  return (
    <div>
      {/* Here, we use the map function, which applies a function to all the elements of an array */}
      {posts.map((post, index) => (
        <React.Fragment key={"post-" + index}>
          <Post {...post} key={`post-` + index} />
          <hr />
        </React.Fragment>
      ))}
    </div>
  );
  // This is similar to using a for loop, and storing all the results
  // We then return the <Post> component for each post, and pass all the keys from the post object, post,
  // to the component as props. We do this by using the spread syntax, which has the same effect as listing
  // all the keys from the object manually as props, as follows:
  // <Post title={p.title} content={p.content} author={p.author} />
  //   let renderedPosts = [];
  //   let i = 0;
  //   for (let p of posts) {
  //     renderedPosts.push(<Post {...p} key={"post-" + i} />);
  //     i++;
  //   }

  //   return <div>{renderedPosts}</div>;
}
