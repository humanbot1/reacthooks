import React, { useState, useContext } from "react";
import { StateContext } from "../contexts";
import { useResource } from "react-request-hook";

export default function CreatePost() {
  const { state, dispatch } = useContext(StateContext);
  const { user } = state;
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

    // create a post request function and use Resouce hook
    // to post the data to backend using post API
  const [, CreatePost] = useResource(({ title, content, author }) => ({
    url: "/posts",
    method: "post",
    data: { title, content, author } // we are chucking the data
  }));

  function handleTitle(evt) {
    setTitle(evt.target.value);
  }

  function handleContent(evt) {
    setContent(evt.target.value);
  }

  function handleCreate() {
    CreatePost({ title, content, author: user }); // call of createPost function with necessary post data to be sent to server
    dispatch({ type: "CREATE_POST", title, content, author: user });
  }

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        handleCreate();
      }}
    >
      <div>
        Author: <b>{user}</b>
      </div>
      <div>
        <label htmlFor="create-title">Title:</label>
        <input
          value={title}
          onChange={handleTitle}
          type="text"
          name="create-title"
          id="create-title"
        />
      </div>
      <textarea value={content} onChange={handleContent} />
      <input
        type="submit"
        value="Create"
        disabled={title.length === 0 || content.length === 0}
      />
    </form>
  );
}
