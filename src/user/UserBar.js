import React, { useContext } from "react";
// eslint-disable-next-line no-unused-vars
import Login from "./Login";

// eslint-disable-next-line no-unused-vars
// Disabling Login import to Lazily load
// import Logout from './Logout';
// eslint-disable-next-line no-unused-vars
import Register from "./Register";
import { StateContext } from "../contexts";

const Logout = React.lazy(() => import("./Logout"));

export default function UserBar() {
  // const [user, setUser] = useState('');
  const { state } = useContext(StateContext);
  const { user } = state;
  if (user) {
    return <Logout />;
  } else {
    return (
      <React.Fragment>
        <Login />
        <Register />
      </React.Fragment>
    );
  }
}
