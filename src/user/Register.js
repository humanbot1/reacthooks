import React, { useState, useContext, useEffect } from "react";
import { StateContext } from "../contexts";
import { useResource } from "react-request-hook";

export default function Register() {
  const { dispatch } = useContext(StateContext);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordRepeat, setPasswordRepeat] = useState("");

  // create post request for user register
  const [user, register] = useResource((username, password) => ({
    url: "/users",
    method: "post",
    data: { username, password }
  }));
  useEffect(() => {
    if (user && user.data) {
      dispatch({ type: "REGISTER", username: user.data.username });
    }
  }, [user]);

  function handleUsername(evt) {
    setUsername(evt.target.value);
  }

  function handlePassword(evt) {
    setPassword(evt.target.value);
  }

  function handlePasswordRepeat(evt) {
    setPasswordRepeat(evt.target.value);
  }

  return (
    <div>
      <h2>Register</h2>
      <form
        onSubmit={e => {
          e.preventDefault();
          register(username, password ); // call register method
        }}
      >
        <label htmlFor="register-username">Username:</label>
        <input
          value={username}
          onChange={handleUsername}
          type="text"
          name="register-username"
          id="register-username"
        />
        <label htmlFor="register-password">Password:</label>
        <input
          value={password}
          onChange={handlePassword}
          type="password"
          name="register-password"
          id="register-password"
        />
        <label htmlFor="register-password-repeat">Repeat password:</label>
        <input
          value={passwordRepeat}
          onChange={handlePasswordRepeat}
          type="password"
          name="register-password-repeat"
          id="register-password-repeat"
        />
        <input
          type="submit"
          value="Register"
          disabled={
            username.length === 0 ||
            password.length === 0 ||
            password !== passwordRepeat
          }
        />
      </form>
    </div>
  );
}
