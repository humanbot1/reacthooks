import React, { useEffect, useState } from "react";
import { useResource } from 'react-request-hook'

// const THEMES = [
//   { primaryColor: "deepskyblue", secondaryColor: "coral" },
//   { primaryColor: "orchid", secondaryColor: "mediumseagreen" },
//   { primaryColor: "tomato", secondaryColor: "darkkhaki" }
// ];

// eslint-disable-next-line no-unused-vars
function ThemeItem({ theme, active, onClick }) {
  return (
    <span
      onClick={onClick}
      style={{
        cursor: "pointer",
        paddingLeft: 8,
        fontWeight: active ? "bold" : "normal"
      }}
    >
      <span style={{ color: theme.primaryColor }}>Primary</span> /{" "}
      <span style={{ color: theme.secondaryColor }}>Secondary</span>
    </span>
  );
}

export default function ChangeTheme({ theme, setTheme }) {
const [themes, getThemes] = useResource(() => ({
    url: '/themes',
    method: 'get',
}))

const { data, isLoading } = themes;

useEffect(getThemes, [])

// The below code has useState that gets initial state from appjs file where its set default
// The below code uses fetch to get theme from backend and then setThemes
//   const [themes, setThemes] = useState([]);
//   useEffect(() => {
//     fetch("api/themes").then(res => res.json()).then(themes => setThemes(themes))
//   }, []);

  function isActive(currentTheme) {
    return (
      currentTheme.primaryColor === theme.primaryColor &&
      currentTheme.secondaryColor === theme.secondaryColor
    );
  }
  return (
    <div>
      Change theme:
      {isLoading && `Loading themes..`}
      {data && data.map((currentTheme, index) => (
        <ThemeItem
          key={"theme-" + index}
          theme={currentTheme}
          active={isActive(currentTheme)}
          onClick={() => setTheme(currentTheme)}
        />
      ))}
    </div>
  );
}
