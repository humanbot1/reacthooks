import axios from 'axios'

// module.exports = () => {

// }

// This type of export should use squiggly bracket while import
// and then chuck this way way {axiosInstance}
export const axiosInstance = axios.create({
    baseURL: 'http://localhost:3000/api/'
})